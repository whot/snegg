# Copyright © 2023 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# This file is formatted with Python black

from typing import Callable, Type, Union

import ctypes
import re

c_base_type = Union[
    ctypes.c_bool,
    ctypes.c_double,
    ctypes.c_int,
    ctypes.c_float,
    ctypes.c_int,
    ctypes.c_int16,
    ctypes.c_int32,
    ctypes.c_int64,
    ctypes.c_int8,
    ctypes.c_size_t,
    ctypes.c_ssize_t,
    ctypes.c_uint,
    ctypes.c_uint16,
    ctypes.c_uint32,
    ctypes.c_uint64,
    ctypes.c_uint8,
]

type_map_t = dict[str, c_base_type]


class CFunc:
    __slots__ = {
        "name",
        "restype",
        "argtypes",
    }

    def __init__(self, declaration: str, typemap: type_map_t):
        self.name, self.argtypes, self.restype = self.parse_declaration(
            declaration, typemap
        )
        # print(self)

    def parse_declaration(
        self, definition: str, typemap: type_map_t
    ) -> tuple[str, tuple[c_base_type, ...], c_base_type]:
        match = re.match(r"(.*?)(\w*)\((.*)\);?", definition)
        assert match is not None, f"Failed to match {definition}"
        restype, name, args = match.groups()
        # print(f"Split to |{restype}|{name}|{args}|")
        argtypes = self.handle_arglist(args, typemap)
        restype = self.map_type(restype, typemap)
        return name, argtypes, restype

    def handle_arglist(self, args: str, typemap: type_map_t) -> tuple[c_base_type, ...]:
        argtypes = []
        for arg in args.split(","):
            arg = arg.lstrip()
            # varargs don't show up in our argument list for the ctype wrapper
            if arg == "...":
                break
            match = re.match(r"(.*?)\s?(\w+)$", arg)
            assert match is not None
            t, _ = match.groups()
            argtypes.append(self.map_type(t, typemap))
        return tuple(argtypes)

    def map_type(self, typestr: str, typemap: type_map_t) -> Type:
        assert typestr, "Type string must not be the empty string"

        # Remove const or static, don't care about those
        typestr = re.sub(r"^\s*", "", typestr)
        typestr = re.sub(r"\s?(const|static)\s?", "", typestr)

        # special case for char *
        if re.match(r"\s?char\s?\*", typestr):
            return ctypes.c_char_p
        # anything else is a void pointer
        if re.match(r".*\*", typestr):
            return ctypes.c_void_p
        # enums have a name in them
        if re.match(r"enum\s+\w+", typestr):
            return ctypes.c_int
        ctypes_map = {
            "bool": ctypes.c_bool,
            "double": ctypes.c_double,
            "enum": ctypes.c_int,
            "float": ctypes.c_float,
            "int": ctypes.c_int,
            "unsigned int": ctypes.c_uint,
            "int16_t": ctypes.c_int16,
            "int32_t": ctypes.c_int32,
            "int64_t": ctypes.c_int64,
            "int8_t": ctypes.c_int8,
            "size_t": ctypes.c_size_t,
            "ssize_t": ctypes.c_ssize_t,
            "uint": ctypes.c_uint,
            "uint16_t": ctypes.c_uint16,
            "uint32_t": ctypes.c_uint32,
            "uint64_t": ctypes.c_uint64,
            "uint8_t": ctypes.c_uint8,
            "void": None,
        }
        typemap = {**ctypes_map, **typemap}
        try:
            return typemap[typestr.strip()]
        except KeyError:
            print(f"Failed to resolve type '{typestr}'")
            raise

    def to_func(self, dll: ctypes.CDLL) -> Callable:
        try:
            func = getattr(dll, self.name)
            func.restype = self.restype
            func.argtypes = self.argtypes
            return func
        except AttributeError:
            import warnings

            warnings.warn(
                f"Function {self.name} is not available, replaced with a noop",
                category=RuntimeWarning,
            )

            def fail(*_):
                raise NotImplementedError()

            return fail

    def __str__(self) -> str:
        return f"CDefinition: {self.restype.__name__ if self.restype else 'void'} {self.name}({','.join((a.__name__ for a in self.argtypes))})"


def clibwrapper(soname: str):
    """
    Class decorator that adds the various `cfunc` attributes as
    methods according to their C declaration. This is a thin wrapper only,
    i.e. the generated methods have their ctypes-based arguments, not
    Python types.

    :param soname: The string name of the soname

    Example::

        @clibwrapper('libc.so')
        class LibC:
            abs = cfunc("int abs(int j)")

        libc = LibC()
        assert libc.abs(-1) == 1
    """

    def wrap(cls):
        cdll = ctypes.CDLL(soname, use_errno=True)
        clsdict = cls.__dict__.copy()
        for name, value in clsdict.items():
            if isinstance(value, CFunc):
                # Remove the attribute and replace it with a function that matches our type
                delattr(cls, name)
                setattr(cls, name, value.to_func(cdll))
        return cls

    return wrap


# Note: invalid type signature which is why we ignore it. During class init we change all
# attributes for the func returned by the CFunc and that func is a callable.
def cfunc(declaration: str, typemap: dict[str, c_base_type] = {}) -> Callable:
    """
    Create a new attribute on a class.

    .. warning:: Does nothing unless the class is decorated with `clibrapper`

    :param declaration: A string that is this function's C declaration. This string is parsed
        and mapped to the respective ctypes arguments.

    """
    return CFunc(declaration, typemap=typemap)  # type: ignore
