#!/bin/bash

dnf install -y meson git gcc gcc-c++ pkgconf-pkg-config systemd-devel libxkbcommon-devel libxml2 python3-attrs python3-jinja2 python3-pyyaml
git clone --depth=1 https://gitlab.freedesktop.org/libinput/libei

pushd libei || exit 1
git log -n1
meson setup _builddir -Dprefix=/usr/ -Dtests=disabled -Ddocumentation=[]
meson configure _builddir
meson compile -C _builddir
meson install -C _builddir
popd || exit 1
