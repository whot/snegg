# Copyright © 2023 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# This file is formatted with Python black

from typing import Type, Union

from snegg import ei

import os
import select
import pytest
import socket
import time


class TestEi:
    @pytest.mark.parametrize("context_type", ("sender", "receiver"))
    @pytest.mark.parametrize("use_env", (True, False))
    def test_context_load(self, monkeypatch, tmp_path, context_type, use_env):
        socketpath = tmp_path / "eis-0"
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.bind(os.fspath(socketpath))
        sock.listen()

        # make sure the socket shows up in the fs
        while not socketpath.exists():
            time.sleep(0.1)

        cls: Union[Type[ei.Sender], Type[ei.Receiver]]

        if context_type == "sender":
            cls = ei.Sender
        else:
            cls = ei.Receiver

        if use_env:
            monkeypatch.setenv("LIBEI_SOCKET", os.fspath(socketpath))
            socketpath = None

        context = cls.create_for_socket(path=socketpath, name=context_type)
        assert context.name == context_type

        # basic properties all contexts have
        now = int(time.clock_gettime(time.CLOCK_MONOTONIC) * 1e6)
        assert context.now >= now
        assert context.fd is not None

        # We have a valid socket, so there shouldn't be any events
        events = list(context.events)
        assert not events, f"Unexpected events in event queue: {events}"

        # Definitely don't expect anything to have happend on the fd
        poll = select.poll()
        poll.register(context.fd)
        context.dispatch()
        assert poll.poll(200) == []

        # Close the server socket
        sock.close()
        context.dispatch()
        events = list(context.events)
        assert len(events) == 1, "Expected to get disconnected"
        event = events.pop(0)
        assert event.event_type == ei.EventType.DISCONNECT
