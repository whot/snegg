#!/usr/bin/env python3

from typing import Optional, Type, Union
import argparse
import logging
import os
import select
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import snegg.ei as ei

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def wait_for_portal():
    import snegg.oeffis

    portal = snegg.oeffis.Oeffis.create()

    poll = select.poll()
    poll.register(portal.fd)
    while poll.poll():
        try:
            if portal.dispatch():
                # We need to keep the portal object alive so we don't get disconnected
                return portal
        except snegg.oeffis.SessionClosedError as e:
            print(f"Closed: {e}", e.message)
            raise SystemExit(1)
        except snegg.oeffis.DisconnectedError as e:
            print(f"Disconnected: {e}", e.message)
            raise SystemExit(1)


def main(use_portal: bool, receiver: bool):
    cls: Type[Union[ei.Receiver, ei.Sender]]
    if receiver:
        cls = ei.Receiver
    else:
        cls = ei.Sender

    if use_portal:
        portal = wait_for_portal()
        ctx = cls.create_for_fd(fd=portal.eis_fd, name="ei-debug-events")
    else:
        ctx = cls.create_for_socket(path=None, name="ei-debug-events")

    pointer: Optional[ei.Device] = None
    abs: Optional[ei.Device] = None
    keyboard: Optional[ei.Device] = None
    touchscreen: Optional[ei.Device] = None

    poll = select.poll()
    poll.register(ctx.fd)
    while poll.poll():
        ctx.dispatch()
        for e in ctx.events:
            print(e)
            if e.event_type == ei.EventType.SEAT_ADDED:
                seat = e.seat
                assert seat is not None
                print(seat)
                seat.bind(seat.capabilities)
            elif e.event_type == ei.EventType.DEVICE_ADDED:
                device = e.device
                assert device is not None
                if (
                    pointer is None
                    and ei.DeviceCapability.POINTER in device.capabilities
                ):
                    pointer = device
                if (
                    abs is None
                    and ei.DeviceCapability.POINTER_ABSOLUTE in device.capabilities
                ):
                    abs = device
                if (
                    keyboard is None
                    and ei.DeviceCapability.KEYBOARD in device.capabilities
                ):
                    keyboard = device
                if (
                    touchscreen is None
                    and ei.DeviceCapability.TOUCH in device.capabilities
                ):
                    touchscreen = device
                print(device)
            elif e.event_type == ei.EventType.DEVICE_RESUMED and not receiver:
                # This is an example only, so let's fire off one event for
                # the more common interfaces and be done with it
                if pointer:
                    pointer.start_emulating().pointer_motion(
                        1, 2
                    ).frame().stop_emulating()
                if abs:
                    abs.start_emulating().pointer_motion_absolute(
                        100, 200
                    ).frame().stop_emulating()
                if keyboard:
                    keyboard.start_emulating().keyboard_key(
                        32, True
                    ).frame().keyboard_key(32, False).frame().stop_emulating()
                if touchscreen:
                    touchscreen.start_emulating()
                    t = touchscreen.touch_new()
                    t.down(500, 500).frame()
                    t.motion(510, 510).frame()
                    t.up().frame()
                    touchscreen.stop_emulating()
            # Event handling for receiver ctx contexts
            elif e.event_type == ei.EventType.KEYBOARD_KEY:
                print(f"{e.key_event}")
            elif e.event_type == ei.EventType.POINTER_MOTION:
                print(f"{e.pointer_event}")
            elif e.event_type == ei.EventType.POINTER_MOTION_ABSOLUTE:
                print(f"{e.pointer_absolute_event}")
            elif e.event_type == ei.EventType.BUTTON_BUTTON:
                print(f"{e.button_event}")
            elif e.event_type == ei.EventType.SCROLL_DELTA:
                print(f"{e.scroll_event}")
            elif e.event_type == ei.EventType.SCROLL_DISCRETE:
                print(f"{e.scroll_discrete_event}")
            elif e.event_type in [
                ei.EventType.SCROLL_STOP,
                ei.EventType.SCROLL_CANCEL,
            ]:
                print(f"{e.scroll_stop_event}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--use-portal", action="store_true", default=False)
    parser.add_argument("--receiver", action="store_true", default=False)
    ns = parser.parse_args()

    try:
        main(use_portal=ns.use_portal, receiver=ns.receiver)
    except KeyboardInterrupt:
        pass
